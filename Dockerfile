FROM alpine:latest

RUN apk add --no-cache bash git jq

COPY update /usr/local/bin

CMD [ "update" ]